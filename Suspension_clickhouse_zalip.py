from clickhouse_driver import Client
import pandas as pd
import pytz
import datetime
import numpy as np
from mysql.connector import MySQLConnection



#НАСТРОЙКИ АЛГОРИТМА
minutes_cycle = 15 #Размер полного цикла(глубина сканирования)
minutes_delta = 15  # Размер временного окна
minutes_refresh = 5 #Частота запуска скрипта внутри цикла

min_points = 20  # Минимальное количество точек для анализа
sensitivity = 1.5  # Чувствительность алгоритма в %
write_db = 1 #Если 1 - писать в БД, если 0 - писать в консоль

# Определение временного окна для запроса
tz = pytz.timezone('Asia/Sakhalin')
sahalin_now = datetime.datetime.now(tz)
time_start = sahalin_now - datetime.timedelta(minutes=24*60)
time_start_string = time_start.strftime("%Y-%m-%d %H:%M:%S")
time_finish = sahalin_now - datetime.timedelta(minutes=0)
time_finish_string = time_finish.strftime("%Y-%m-%d %H:%M:%S")

# Подключение к Clickhouse
client = Client('DT-Click01.sahalin.local')

# Подключение к локальной БД
if (write_db==1):
    mydb = MySQLConnection(
        user="admin",
        password="OBXrEr2S",
        host="mes-eva01.sahalin.local",
        port=3306,
        database = "database"
    )
    cur = mydb.cursor()

#Количество итераций внутри цикла
glob_iter = int(minutes_cycle / minutes_refresh)

# Получение списка БелАЗ-75306 (ObjectID)
Belaz_list = client.execute('SELECT DISTINCT(ObjectID) FROM vgk.BelAZ75306;', settings={"use_numpy": True})
Belaz_list = pd.DataFrame(Belaz_list, columns=['ObjectID'])
Belaz_list = Belaz_list[Belaz_list['ObjectID'] != 0]

# Для каждого БелАЗа выгружаем данные за последние minutes_delta минут по Скорости, Весу и Силе тока в подвесках
for i_belaz in Belaz_list['ObjectID'].unique():
    data_belaz = client.execute(
        'SELECT dtime_read, wln_speedGPS, wln_weight, wln_pres1, wln_pres2, wln_pres3, wln_pres4, wln_name FROM vgk.BelAZ75306 WHERE (dtime_read > ' + "'" + time_start_string + "'" + ') AND (dtime_read < ' + "'" + time_finish_string + "'" + ') AND (ObjectID = ' + str(
            i_belaz) + ') ORDER BY dtime_read DESC',
        settings={"use_numpy": True})

    if len(data_belaz) > 0:
        data_belaz = pd.DataFrame(data_belaz,
                                  columns=['time', 'wln_speedGPS', 'wln_weight', 'wln_pres1', 'wln_pres2',
                                           'wln_pres3',
                                           'wln_pres4', 'wln_name'])
        garaj_belaz = data_belaz['wln_name'].unique()[0]  # получение номера белаза
        # print(name_belaz)
        data_belaz = data_belaz[
            ['time', 'wln_weight', 'wln_speedGPS', 'wln_pres1', 'wln_pres2', 'wln_pres3', 'wln_pres4']]
        data_belaz.rename(
            columns={'wln_pres1': 'Правая передняя', 'wln_pres2': 'Левая передняя',
                     'wln_pres3': 'Правая задняя',
                     'wln_pres4': 'Левая задняя'}, inplace=True)

        for i_col in ['Правая передняя', 'Левая передняя', 'Правая задняя', 'Левая задняя']:

            event_cur = 0  # Событий за последний цикл не было

            for iter in range(0, glob_iter):
                time_start_loc = sahalin_now - datetime.timedelta(minutes=minutes_refresh * iter + minutes_delta)
                time_finish_loc = sahalin_now - datetime.timedelta(minutes=minutes_refresh * iter)
                time_start_loc = time_start_loc.replace(tzinfo=None)
                time_finish_loc = time_finish_loc.replace(tzinfo=None)
                data_belaz_anomaly = data_belaz[
                    (data_belaz['time'] > time_start_loc) & (data_belaz['time'] < time_finish_loc)]
                data_belaz_anomaly = data_belaz_anomaly.loc[(data_belaz_anomaly['wln_speedGPS'] > 6.0)]
                data_belaz_anomaly = data_belaz_anomaly.reset_index(drop=True)

                ##СЮДА МОЖНО ЗАНОСИТЬ ПРАВИЛА ПО КАЖДОЙ ПОДВЕСКЕ И ВЫДАЧУ СИГНАЛИЗАЦИЙ И СОБЫТИЙ
                if (data_belaz_anomaly.empty == False) and (data_belaz_anomaly.shape[0] > min_points):
                    # Проверка завис ли датчик
                    if (data_belaz_anomaly[i_col].max() == data_belaz_anomaly[i_col].mean()) and (
                            data_belaz_anomaly[i_col].min() == data_belaz_anomaly[i_col].mean()):
                        if data_belaz_anomaly[i_col].mean()==0.0:
                            event_cur = 1  # нулевое зависание датчика
                        elif data_belaz_anomaly[i_col].mean()>0.0:
                            event_cur = 2  # ненулевое зависание датчика


            if event_cur > 0:
                event_name = ['Отсутствие дефекта','Зависание датчика на нуле', 'Зависание датчика больше нуля']
                if (write_db == 1):
                    sql = """INSERT INTO suspention_anomalies (date, name_belaz, i_col, status, garaj_belaz) VALUES (%s, %s, %s, %s, %s)"""
                    val = (time_finish_string, str(i_belaz), i_col, event_name[event_cur], garaj_belaz)
                    cur.execute(sql, val)
                    mydb.commit()
                    print(time_finish_string, str(i_belaz), i_col, event_name[event_cur], garaj_belaz)
                else:
                    print(time_finish_string, garaj_belaz, i_belaz, i_col, event_name[event_cur], garaj_belaz)