from clickhouse_driver import Client
import pandas as pd
import pytz
import datetime
import numpy as np
from mysql.connector import MySQLConnection

#НАСТРОЙКИ АЛГОРИТМА
minutes_delta = 15  # Размер временного окна (глубина сканирования)
min_points = 20  # Минимальное количество точек для анализа
sensitivity = 1.5  # Чувствительность алгоритма в %
write_db = 1 #Если 1 - писать в БД, если 0 - писать в консоль

# Определение временного окна для запроса
tz = pytz.timezone('Asia/Sakhalin')
sahalin_now = datetime.datetime.now(tz)
print(sahalin_now)
time_start = sahalin_now - datetime.timedelta(minutes=minutes_delta)
time_start_string = time_start.strftime("%Y-%m-%d %H:%M:%S")
time_finish = sahalin_now - datetime.timedelta(minutes=0)
time_finish_string = time_finish.strftime("%Y-%m-%d %H:%M:%S")

# Подключение к Clickhouse
client = Client('DT-Click01.sahalin.local')

# Подключение к локальной БД
if (write_db==1):
    mydb = MySQLConnection(
        user="admin",
        password="OBXrEr2S",
        host="mes-eva01.sahalin.local",
        port=3306,
        database = "database"
    )
    cur = mydb.cursor()


# Получение списка БелАЗ-75306 (ObjectID)
Belaz_list = client.execute('SELECT DISTINCT(ObjectID) FROM vgk.BelAZ75306;', settings={"use_numpy": True})
Belaz_list = pd.DataFrame(Belaz_list, columns=['ObjectID'])
Belaz_list = Belaz_list[Belaz_list['ObjectID'] != 0]


# Для каждого БелАЗа выгружаем данные за последние minutes_delta минут по Скорости, Весу и Силе тока в подвесках
for i_belaz in Belaz_list['ObjectID'].unique():
    data_belaz = client.execute(
        'SELECT dtime_read, wln_speedGPS, wln_weight, wln_pres1, wln_pres2, wln_pres3, wln_pres4, wln_name FROM vgk.BelAZ75306 WHERE (dtime_read > ' + "'" + time_start_string + "'" + ') AND (dtime_read < ' + "'" + time_finish_string + "'" + ') AND (ObjectID = ' + str(
            i_belaz) + ') ORDER BY dtime_read DESC',
        settings={"use_numpy": True})

    if len(data_belaz) > 0:
        data_belaz = pd.DataFrame(data_belaz,
                                  columns=['time', 'wln_speedGPS', 'wln_weight', 'wln_pres1', 'wln_pres2',
                                           'wln_pres3',
                                           'wln_pres4', 'wln_name'])
        garaj_belaz = data_belaz['wln_name'].unique()[0]  # получение номера белаза
        # print(name_belaz)

        # Анализируется  случая - 0. Берется только период движения самосвала, вес груза не учитывается 1. Берется только период непрерывного движения груженого самосвала
        data_belaz_anomaly = data_belaz
        data_belaz_anomaly['targ'] = 0.0
        data_belaz_anomaly = data_belaz_anomaly.loc[(data_belaz_anomaly['wln_weight'] >= 50.0) & (
                data_belaz_anomaly['wln_speedGPS'] > 6.0)]
        data_belaz_anomaly = data_belaz_anomaly.reset_index(drop=True)

        # При наличии данных осуществляется диагностика подвесок
        if (data_belaz_anomaly.empty == False) and (data_belaz_anomaly.shape[0] > min_points):
            # print(data_belaz)
            data_belaz_anomaly = data_belaz_anomaly[['wln_pres1', 'wln_pres2', 'wln_pres3', 'wln_pres4']]
            data_belaz_anomaly.rename(
                columns={'wln_pres1': 'Правая передняя', 'wln_pres2': 'Левая передняя',
                         'wln_pres3': 'Правая задняя',
                         'wln_pres4': 'Левая задняя'}, inplace=True)

            for i_col in data_belaz_anomaly.columns:

                ##СЮДА МОЖНО ЗАНОСИТЬ ПРАВИЛА ПО КАЖДОЙ ПОДВЕСКЕ И ВЫДАЧУ СИГНАЛИЗАЦИЙ И СОБЫТИЙ

                # Если датчик завис
                if (data_belaz_anomaly[i_col].max() <= (
                        data_belaz_anomaly[i_col].mean() * (1.0 + (sensitivity / 100.0))) or (
                              data_belaz_anomaly[i_col].min() >= data_belaz_anomaly[i_col].mean() * (
                              1.0 - (sensitivity / 100.0))))  and (
                        data_belaz_anomaly[i_col].min() != 0.0):
                    if (write_db == 1):
                        sql = """INSERT INTO suspention_anomalies (date, name_belaz, i_col, status, garaj_belaz) VALUES (%s, %s, %s, %s, %s)"""
                        val = (time_finish_string, str(i_belaz), i_col, 'Аномалия в ПГП', garaj_belaz)
                        cur.execute(sql, val)
                        mydb.commit()
                    else:
                        print(time_finish_string, garaj_belaz, i_belaz, i_col,
                              'Аномалия в ПГП')  # По этим данным можно записывать событие - time_finish_string (время в формате строки %Y-%m-%d %H:%M:%S сахалинская временная зона), i_belaz - Object ID в Clickhouse, i_col - подвеска, тип сигнализации
